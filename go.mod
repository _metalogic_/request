module bitbucket.org/_metalogic_/request

go 1.13

require (
	github.com/bitly/go-simplejson v0.5.0
	github.com/bmizerany/assert v0.0.0-20120716205630-e17e99893cb6
	github.com/kr/pretty v0.0.0-20160325215624-add1dbc86daf
	github.com/kr/text v0.0.0-20160504234017-7cafcd837844
	github.com/mozillazg/request v0.8.0
	golang.org/x/net v0.0.0-20160531150152-c4c3ea71919d
)
